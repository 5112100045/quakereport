package com.example.android.quakereport;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by alif.sip on 30/8/2016.
 */
public class NearMeFragment extends Fragment {

    private static final String USGS_REQUEST_URL =
            "http://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&endtime&limit=1&minmagnitude=4";

    public NearMeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EarthquakeAsyncTask earthquakeAsyncTask = new EarthquakeAsyncTask();
        earthquakeAsyncTask.execute(USGS_REQUEST_URL);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.near_me_fragment, container, false);
    }

    private void updateUi(Event earthquake) {
        TextView titleTextView = (TextView) getView().findViewById(R.id.title);
        titleTextView.setText(earthquake.title);

        TextView tsunamiTextView = (TextView) getView().findViewById(R.id.number_of_people);
        tsunamiTextView.setText(getString(R.string.num_people_felt_it, earthquake.numOfPeople));

        TextView magnitudeTextView = (TextView) getView().findViewById(R.id.perceived_magnitude);
        magnitudeTextView.setText(earthquake.perceivedStrength);
    }

    private class EarthquakeAsyncTask extends AsyncTask<String, Void, Event> {

        @Override
        protected Event doInBackground(String... strings) {
            // Perform the HTTP request for earthquake data and process the response.

            if (strings.length < 1 || strings[0] == null) {
                return null;
            }

            Event earthquake = Utils.fetchEarthquakeData(strings[0]);
            return earthquake;
        }

        @Override
        protected void onPostExecute(Event event) {
            // Update the information displayed to the user.
            if (event == null) {
                return;
            }
            updateUi(event);
        }
    }
}
